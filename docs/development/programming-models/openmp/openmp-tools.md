# Tools for OpenMP

Tools for tuning OpenMP codes to get better performance include:

* [Use Cray Reveal to Insert OpenMP Directives](#use-cray-reveal-to-insert-openmp-directives)
* [Use Codee to Insert OpenMP Directives](#use-codee-to-insert-openmp-directives)

## Use Cray Reveal to Insert OpenMP Directives

Reveal is an integrated performance analysis and code optimization tool.

Reveal provides:

* Source code navigation using the analysis data of the entire program
* Performance data collected during program execution
* Dependency information for targeted loops
* Variable scoping feedback and suggested compiler directives

For information on how to use Cray Reveal on NERSC systems,
visit the page on [Reveal](../../../tools/performance/reveal/index.md).

## Use Codee to Insert OpenMP Directives

Codee is a suite of command-line tools that automates code inspection from the
performance perspective. Codee scans the source code without executing it and
can detect and fix defects related to parallelism with OpenMP and OpenACC.
It can also identify opportunities and insert compiler directives for
OpenMP/OpenACC parallelization on CPUs and GPUs.

For information on how to use Codee on NERSC systems,
visit the page on [Codee](../../../tools/performance/codee/index.md).
