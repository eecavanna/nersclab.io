cori$  module show mana 
-------------------------------------------------------------------
zz217@cori07:~> module show mana
-------------------------------------------------------------------
/global/common/software/nersc/cle7up03/extra_modulefiles/mana/2022-10-10:

module-whatis	 MANA (MPI-Agnostic Network-Agnostic) transparent checkpointing tool, 
                 implemented in DMTCP (Distributed MultiThreaded Checkpointing) 
                 transparently checkpoints MPI applications in user-space -- with 
                 no modifications to user code or to the O/S. 

setenv		 MANA_ROOT /usr/common/software/mana/2022-10-10/intel 
setenv		 DMTCP_DL_PLUGIN 0 
setenv           PMI_NO_PREINITIALIZE 1
prepend-path	 LD_LIBRARY_PATH /usr/common/software/mana/2022-10-10/intel/lib/dmtcp 
prepend-path	 PATH . 
prepend-path	 PATH /usr/common/software/mana/2022-10-10/intel/bin 
prepend-path	 MANPATH /usr/common/software/mana/2022-10-10/intel/share/man 
setenv		 MANA_DIR /usr/common/software/mana/2022-10-10/intel 
setenv		 SITE_MODULE_NAMES mana 
-------------------------------------------------------------------
