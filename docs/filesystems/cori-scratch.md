# Cori SCRATCH

!!! warning "Cori retired on May 31, 2023 at noon"
    	Cori's scratch file system was retired along with Cori. Please
    	refer to the [Migrating from Cori to
    	Perlmutter](../systems/cori/migrate_to_perlmutter.md) page for the
    	detailed Cori retirement plan and information about migrating
    	your applications to Perlmutter.

Cori scratch was a Lustre file system designed for high performance
temporary storage of large files. It had 30 PB of disk space, an
aggregate bandwidth of >700 GB/sec I/O bandwidth, and was made up of
10000+ disks and 248 I/O servers called OSTs.
